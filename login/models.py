from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.
class User(models.Model):
    # Primary Key
    username = models.CharField(max_length=120, default='user', primary_key=True)
    password = models.CharField(max_length=120, default='password', unique=False, primary_key=False)

    # Others
    name = models.CharField(max_length=120, default='anonymous')
    email = models.EmailField(default='example@mail.com', unique=True)
    is_teacher = models.BooleanField(default=False)

    def __str__(self):
        return self.username

    def as_json(self):
        return dict(
            username = self.username,
            name = self.name,
            email = self.email,
            is_teacher = self.is_teacher,
        )