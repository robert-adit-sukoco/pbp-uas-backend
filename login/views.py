from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from main.views import SUCCESS, BAD_REQUEST, WRONG_CREDENTIALS
from .models import User

# Create your views here.
@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        try:
            username = request.GET.get('username')
            password = request.GET.get('password')
            get_obj = User.objects.get(pk=username)

            if (compare_password(get_obj.password, password)):
                return SUCCESS
            return WRONG_CREDENTIALS

        except:
            return BAD_REQUEST
    return BAD_REQUEST


@api_view(['POST'])
def register(request):
    if request.method == 'POST':
        try:
            uname = request.POST.get('uname')
            pw = request.POST.get('pw')
            name = request.POST.get('name')
            email = request.POST.get('email')
            ist = request.POST.get('ist')

            new_user = User(username=uname, password=pw, name=name, email=email, is_teacher=ist)
            new_user.save()
            return SUCCESS
        except: 
            return BAD_REQUEST
    return BAD_REQUEST


def compare_password(compare_input, expected):
    return compare_input == expected
