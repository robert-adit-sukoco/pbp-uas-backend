# Backend for Tugas Akhir PBP 2021


## GET

### Get all
1. Get all users (https://pbp-uas-backend.herokuapp.com/apiuser/)
2. Get all subjects (https://pbp-uas-backend.herokuapp.com/apisubject/)
3. Get all tasks (https://pbp-uas-backend.herokuapp.com/apitask/)
4. Get all submissions (https://pbp-uas-backend.herokuapp.com/apisubmission/)
   
### Get certain
1. Get certain user (https://pbp-uas-backend.herokuapp.com/apiuser/{user_id})
2. Get certain subject (https://pbp-uas-backend.herokuapp.com/apisubject/{subject_id})
3. Get certain task (https://pbp-uas-backend.herokuapp.com/apitask/{task_id})
4. Get certain submission (https://pbp-uas-backend.herokuapp.com/apisubmission/{submission_id})
   
### Generate id
1.  Generate new id for new subject (Use this when posting new subject to database) (https://pbp-uas-backend.herokuapp.com/request_subject_id)
2. Generate new id for new task (Use this when posting new subject to database) (https://pbp-uas-backend.herokuapp.com/request_task_id?subject_id={subject_id})
3. Generate new id for new submission (Use this when posting new subject to database) (https://pbp-uas-backend.herokuapp.com/request_submission_id?task_id={task_id}&student_id={student_id})


### Student Gets
1.  Get a student's subjects (https://pbp-uas-backend.herokuapp.com/get_students_subjects?sid={student_id})
2.  Get a student's submissions (https://pbp-uas-backend.herokuapp.com/get_students_submissions?student_id={student_id})

### Subject Gets
1.  Get a subject's students (https://pbp-uas-backend.herokuapp.com/get_subjects_students?subject_id={subject_id})
2.  Get a subject's tasks (https://pbp-uas-backend.herokuapp.com/get_subjects_tasks?subject_id={subject_id})

### Teacher Gets
1.  Get a teacher's subjects (https://pbp-uas-backend.herokuapp.com/get_teachers_subjects?teacher_id={teacher_id}

### Task Gets
1. Get a task's submissions (https://pbp-uas-backend.herokuapp.com/get_tasks_submissions?task_id={task_id})


## POST

1. Create new subject (https://pbp-uas-backend.herokuapp.com/create_subject?s_id={new_subject_id}&t_id={teacher_id}&name={new_subject_name})
2. Create new task (https://pbp-uas-backend.herokuapp.com/create_task?task_id={new_task_id}&subjectid={subject_fk}&desc={new_desc}&dd={dl_date}&mm={dl_month}&yy={dl_year}&hh={dl_hour}&mm={dl_minute}&ss={dl_second})
3. Create new submission (https://pbp-uas-backend.herokuapp.com/create_subject?submit_id={new_submission_id}&student_id={student_id}&task_id={task_id}&description={description_ifneeded})


## PUT
1. Update task (https://pbp-uas-backend.herokuapp.com/edit_task?task_id={task_id}&task_name={task_name}&desc={new_description}&dd={new_dl_date}&mm={new_dl_month}&yy={new_dl_year}&hh={new_dl_hour}&mm={new_dl_minute}&ss={new_dl_second})
2. Update submission's grade (https://pbp-uas-backend.herokuapp.com/edit_submission?submission_id={submission_id}&new_grade={new_grade_input})


## DELETE
1. Delete task (https://pbp-uas-backend.herokuapp.com/delete_task?task_id={task_id})


## Login
1. Login (https://pbp-uas-backend.herokuapp.com/login?username={username_input}&password={password_input}), will return status code 200 if login is successful, 401 if credentials don't match, 400 if something else went wrong


## License
Template from https://gitlab.com/laymonage/django-template-heroku

