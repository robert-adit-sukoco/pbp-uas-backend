from django.contrib import admin
from .models import Subject, Task, Submission

# Register your models here.
admin.site.register(Subject)
admin.site.register(Task)
admin.site.register(Submission)