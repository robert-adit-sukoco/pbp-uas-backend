from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from login.models import User
from django.utils import timezone

# Create your models here.    
class Subject(models.Model):
    # Primary Key
    subject_id = models.CharField(max_length=30, default='subject', primary_key=True)

    # Foreign Key
    teacher_id = models.ForeignKey(User, default='subject', on_delete=models.CASCADE, related_name='subject2teacher')

    # Others
    name = models.CharField(max_length=120, default='subject')
    students = models.ManyToManyField(User, related_name='subject2student', blank=True)

    def __str__(self):
        return self.name

    def as_json(self):
        return dict(
            subject_id = self.subject_id,
            teacher_id = self.teacher_id,
            name = self.name,
            students= self.students,
        )


class Task(models.Model):
    # Primary Key
    task_id = models.CharField(max_length=30, default='subject', primary_key=True)

    # Foreign Key
    subject_fkey = models.ForeignKey(Subject, default='subject', on_delete=models.CASCADE)

    # Others
    task_name = models.CharField(max_length=30, default='task_name')
    description = models.CharField(max_length=200, default='subject')
    deadline = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.task_name


class Submission(models.Model):
    # Primary Key
    submission_id = models.CharField(max_length=30, default='subject', primary_key=True)

    # Foreign Keys
    submitted_by = models.ForeignKey(User, default='subject', on_delete=models.CASCADE)
    task_fkey = models.ForeignKey(Task, default='subject', on_delete=models.CASCADE)

    # Others
    submit_time = models.DateTimeField(default=timezone.now, editable=True)
    file = models.FileField(default='subject', editable=True)
    description = models.CharField(max_length=200,  default='subject', editable=True)
    is_graded = models.BooleanField(default=False, editable=True)
    grade = models.IntegerField(default=0, validators=[MaxValueValidator(100), MinValueValidator(0)], editable=True)

