from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.utils.timezone import make_aware
from django.forms.models import model_to_dict
from .models import User, Subject, Task, Submission
import pytz, datetime
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


SYSTEM_TIMEZONE = pytz.timezone('Asia/Jakarta')

SUCCESS_INT = status.HTTP_200_OK
SUCCESS = Response(status=SUCCESS_INT)

BAD_REQUEST_INT = status.HTTP_400_BAD_REQUEST
BAD_REQUEST = Response(status=BAD_REQUEST_INT)

WRONG_CREDENTIALS_INT = status.HTTP_401_UNAUTHORIZED
WRONG_CREDENTIALS = Response(status=WRONG_CREDENTIALS_INT)


def home(request):
    return render(request, 'main/home.html')


@api_view(['GET'])
def get_student_subjects(request):
    try:
        get_id = request.GET.get('sid')
        get_student_obj = User.objects.get(pk=get_id)
        obj_list = list()
        for subject in Subject.objects.all():
            if get_student_obj in subject.students.all():
                obj_list.append(subject.subject_id)
        get_data = Subject.objects.filter(subject_id__in=obj_list).values()
        return Response(get_data, status=SUCCESS_INT)

    except:
        return BAD_REQUEST


@api_view(['GET'])
def get_subjects_student(request):
    try:
        get_id = request.GET.get('subject_id')
        get_subject_obj = Subject.objects.get(pk=get_id)
        get_data = get_subject_obj.students.all().values()
        return Response(get_data, status=SUCCESS_INT)
    except Exception as e:
        return BAD_REQUEST


@api_view(['GET'])
def get_subjects_tasks(request):
    try:
        get_id = request.GET.get('subject_id')
        get_subject_obj = Subject.objects.get(pk=get_id)
        get_data = Task.objects.filter(subject_fkey=get_subject_obj).values()
        return Response(get_data, status=SUCCESS_INT)
    except:
        return BAD_REQUEST


@api_view(['GET'])
def get_tasks_submissions(request):
    try:
        get_tid = request.GET.get('task_id')
        get_task_obj = Task.objects.get(pk=get_tid)
        get_data = Submission.objects.filter(task_fkey=get_task_obj).values()
        return Response(get_data, status=SUCCESS_INT)
    except:
        return BAD_REQUEST


@api_view(['GET'])
def get_student_submissions(request):
    try:
        get_sid = request.GET.get('student_id')
        get_student_obj = User.objects.get(pk=get_sid)
        if (get_student_obj.is_teacher):
            return BAD_REQUEST
        get_data = Submission.objects.filter(submitted_by=get_student_obj)
        return Response(get_data, status=SUCCESS_INT)
    except:
        return BAD_REQUEST


@api_view(['GET'])
def get_teacher_subjects(request):
    try:
        get_tid = request.GET.get('teacher_id')
        get_teacher_obj = User.objects.get(pk=get_tid)
        if (not get_teacher_obj.is_teacher):
            return BAD_REQUEST
        get_data = Subject.objects.filter(teacher_id=get_teacher_obj).values()
        return Response(get_data, status=SUCCESS_INT)
    except:
        return BAD_REQUEST
    

@api_view(['GET'])
def get_new_subject_id(request):
    return Response({'new_id': f"subject{str(Subject.objects.count() + 1).zfill(2)}"}, status=SUCCESS_INT)

@api_view(['GET'])
def get_new_task_id(request):
    try:
        get_subject_id = request.GET.get('subject_id')
        get_subject_obj = Subject.objects.get(pk=get_subject_id)
        return Response({'new_id': f"{get_subject_id}-task{str(Task.objects.filter(subject_fkey=get_subject_obj).count() + 1).zfill(2)}"}, status=SUCCESS_INT)
    except:
        return BAD_REQUEST

@api_view(['GET'])
def get_new_submission_id(request):
    try:
        get_task_id = request.GET.get('task_id')
        get_student_id = request.GET.get('student_id')

        task_obj = Task.objects.get(pk=get_task_id)
        student_obj = User.objects.get(pk=get_student_id)

        if (student_obj.is_teacher):
            return BAD_REQUEST

        return Response({'new_id': f"{get_task_id}-submission{str(Submission.objects.filter(task_fkey=task_obj).count() + 1).zfill(2)}"}, status=SUCCESS_INT)
    except:
        return BAD_REQUEST


@api_view(['POST'])
def create_subject(request):
    try:
        # Get query
        s_id = request.GET.get('s_id')
        t_id = request.GET.get('t_id')
        get_name = request.GET.get('name')

        # Get teacher object
        teacher_obj = User.objects.get(pk=t_id)
        if (not teacher_obj.is_teacher):
            return BAD_REQUEST

        # Create new object and save to database
        new_subject = Subject(subject_id=s_id, teacher_id=teacher_obj, name=get_name)
        new_subject.save()
        return SUCCESS
    except:
        return BAD_REQUEST


@api_view(['POST'])
def create_task(request):
    try:
        # Primary Key
        t_id = request.GET.get('task_id', '')

        # Foreign Key
        s_key = request.GET.get('subjectid', '')

        # Others
        desc = request.GET.get('desc', '-')
        dl_date = request.GET.get('dd')
        dl_month = request.GET.get('mm')
        dl_year = request.GET.get('yy')
        dl_hour = request.GET.get('hh')
        dl_minute = request.GET.get('mm')
        dl_second = request.GET.get('ss')

        deadline_obj = datetime(int(dl_year), int(dl_month), int(dl_date), int(dl_hour), int(dl_minute), int(dl_second))
        deadline_obj = make_aware(deadline_obj, timezone=SYSTEM_TIMEZONE)

        subject_obj = Subject.objects.get(pk=s_key)

        new_task = Task(task_id=t_id, subject_fkey=subject_obj, description=desc, deadline=deadline_obj)
        new_task.save()
        return SUCCESS
    except:
        return BAD_REQUEST


@api_view(['POST'])
def create_submission(request):
    try:
        get_submit_id = request.GET.get('submit_id')
        get_student_id = request.GET.get('student_id')
        get_task_id = request.GET.get('task_id')
        get_description = request.GET.get('description', '-')

        file_obj = None

        get_student_obj = User.objects.get(pk=get_student_id)
        get_task_obj = User.objects.get(pk=get_task_id)

        if (get_student_obj.is_teacher):
            return BAD_REQUEST

        submission_time_obj = pytz.timezone.now

        new_submission = Submission(submission_id=get_submit_id, submitted_by=get_student_obj, task_fkey=get_task_obj, submit_time=submission_time_obj, file=file_obj, description=get_description, is_graded=False, grade=0)
        new_submission.save()
        return SUCCESS
    except:
        return BAD_REQUEST


@api_view(['DELETE'])
def delete_task(request):
    try:
        task = Task.objects.get(pk=request.GET.get('task_id'))
        task.delete()
        return SUCCESS
    except:
        return BAD_REQUEST


@api_view(['PUT'])
def edit_task(request):
    try:
        # Primary Key
        t_id = request.GET.get('task_id', '')

        # Others
        taskname = request.GET.get('task_name')
        desc = request.GET.get('desc', '-')
        dl_date = request.GET.get('dd')
        dl_month = request.GET.get('mm')
        dl_year = request.GET.get('yy')
        dl_hour = request.GET.get('hh')
        dl_minute = request.GET.get('mm')
        dl_second = request.GET.get('ss')

        deadline_obj = datetime(int(dl_year), int(dl_month), int(dl_date), int(dl_hour), int(dl_minute), int(dl_second))
        deadline_obj = make_aware(deadline_obj, timezone=SYSTEM_TIMEZONE)

        edited_task = Task.objects.get(pk=t_id)
        edited_task.task_name = taskname
        edited_task.deadline = deadline_obj
        edited_task.description = desc
        edited_task.save()
        return SUCCESS
    except:
        return BAD_REQUEST


@api_view(['PUT'])
def edit_submission(request):
    try:
        get_id = request.GET.get('submission_id')
        get_new_grade = request.GET.get('new_grade')
        edited_submission = Submission.objects.get(pk=get_id)
        edited_submission.grade = int(get_new_grade)
        edited_submission.is_graded = True
        edited_submission.save()
        return SUCCESS
    except:
        return BAD_REQUEST