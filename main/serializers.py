from rest_framework import serializers
from .models import Subject, Task, Submission
from login.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'name', 'email', 'is_teacher']


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'password']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['subject_id', 'teacher_id', 'name', 'students']


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['task_id', 'subject_fkey', 'task_name', 'description', 'deadline']


class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ['submission_id', 'submitted_by', 'task_fkey', 'submit_time', 'file', 'description', 'is_graded', 'grade']



