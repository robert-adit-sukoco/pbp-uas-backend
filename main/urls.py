from django.urls import path, include
from rest_framework import routers
from .viewset_api import *

from . import views

app_name = 'main'

router = routers.DefaultRouter()
router.register('user', UserViewSet, basename='user')
router.register('subject', SubjectViewSet, basename='subject')
router.register('task', TaskViewSet, basename='task')
router.register('submission', SubmissionViewSet, basename='submission')

urlpatterns = [
    path('', views.home, name='home'),
    path('api', include(router.urls)),

    # New ID Methods
    path('request_subject_id', views.get_new_subject_id, name='request_subject_id'),
    path('request_task_id', views.get_new_task_id, name='request_task_id'),
    path('request_submission_id', views.get_new_submission_id, name='request_submission_id'),

    # GET methods
    path('get_students_subjects', views.get_student_subjects, name='get_students_subjects'),
    path('get_students_submissions', views.get_student_submissions, name='get_students_subjects'),
    path('get_subjects_students', views.get_subjects_student, name='get_subjects_students'),
    path('get_subjects_tasks', views.get_subjects_tasks, name='get_subjects_tasks'),
    path('get_teachers_subjects', views.get_teacher_subjects, name='get_teachers_subjects'),
    path('get_tasks_submissions', views.get_tasks_submissions, name='get_task_submissions'),

    # POST methods
    path('create_task', views.create_task, name='create_task'),
    path('create_subject', views.create_subject, name='create_subject'),
    path('create_submission', views.create_submission, name='create_submission'),

    # PUT methods
    path('edit_task', views.edit_task, name='edit_task'),
    path('edit_submission', views.edit_submission, name='edit_submission'),

    # DELETE methods
    path('delete_task', views.delete_task, name='delete_task'),
]
